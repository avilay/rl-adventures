import numpy as np
import torch as t
from torch.nn.functional import one_hot, normalize
from torch.distributions import Categorical
from collections import deque
import gym

np.set_printoptions(precision=3)

GAMMA = 0.99
LAMDA = 0.97
STEPS_PER_EPOCH = 4000
MAX_STEPS_PER_EP = 500
ACTOR_LR = 0.0003
EPOCHS = 200
CRITIC_LR = 0.001
CRITIC_ITERS = 80


class Policy(t.nn.Module):
    def __init__(self):
        super().__init__()
        self.net = t.nn.Sequential(
            t.nn.Linear(in_features=4, out_features=64, bias=True),
            t.nn.Tanh(),
            t.nn.Linear(in_features=64, out_features=64, bias=True),
            t.nn.Tanh(),
            t.nn.Linear(in_features=64, out_features=2),
            t.nn.Softmax(),
        )

    def forward(self, states):
        norm_states = normalize(states, dim=1)
        probs = self.net(norm_states)
        return probs

    def sample(self, state):
        batch_of_one = t.tensor([state], dtype=t.float)
        probs = self.forward(batch_of_one)
        action = Categorical(probs=probs).sample().item()
        return action


def policy_loss_fn(prob_dist, actions, value_ests):
    one_hot_actions = one_hot(actions, num_classes=2)
    probs = t.sum(one_hot_actions * prob_dist, axis=1)
    return -t.mean(t.log(probs) * value_ests)


class ValueFunc(t.nn.Module):
    def __init__(self):
        super().__init__()
        self.net = t.nn.Sequential(
            t.nn.Linear(in_features=4, out_features=64, bias=True),
            t.nn.Tanh(),
            t.nn.Linear(in_features=64, out_features=64, bias=True),
            t.nn.Tanh(),
            t.nn.Linear(in_features=64, out_features=1),
        )

    def forward(self, states):
        norm_states = normalize(states, dim=1)
        svals = self.net(norm_states)
        return svals


def calc_disc_returns(rewards, gamma):
    disc_cumsums = [None] * len(rewards)
    disc_cumsums[-1] = rewards[-1]
    for i in range(len(rewards) - 2, -1, -1):
        disc_cumsums[i] = rewards[i] + gamma * disc_cumsums[i + 1]
    return disc_cumsums


def calc_gaes(value_func, states, rewards, last_state=None):
    with t.no_grad():
        svals = value_func(t.tensor(states, dtype=t.float)).numpy().squeeze(axis=1)
        if last_state is None:
            last_val = [0.0]
        else:
            last_val = value_func(t.tensor([last_state], dtype=t.float)).numpy().squeeze(axis=1)
    svals = np.concatenate((svals, last_val))
    deltas = np.array(rewards) + GAMMA * svals[1:] - svals[:-1]
    gaes = calc_disc_returns(deltas, GAMMA * LAMDA)
    return gaes


def main():
    env = gym.make("CartPole-v1")
    rewards = deque([], maxlen=100)

    policy = Policy()
    policy_optim = t.optim.Adam(params=policy.parameters(), lr=ACTOR_LR)

    value_func = ValueFunc()
    value_func_optim = t.optim.Adam(params=value_func.parameters(), lr=CRITIC_LR)
    value_loss_fn = t.nn.MSELoss(reduction="mean")

    for epoch in range(EPOCHS):
        # Gather episodes for this epoch
        states, actions, disc_returns, gaes = [], [], [], []
        ep_states, ep_rewards = [], []
        state = env.reset()
        reward = 0
        policy.eval()
        value_func.eval()
        for time_step in range(STEPS_PER_EPOCH):
            action = policy.sample(state)

            ep_states.append(state)
            actions.append(action)
            ep_rewards.append(reward)

            state, reward, done, _ = env.step(action)

            if done or len(ep_rewards) == MAX_STEPS_PER_EP:
                rewards.append(np.sum(ep_rewards))

                ep_disc_returns = calc_disc_returns(ep_rewards, GAMMA)
                disc_returns += ep_disc_returns

                ep_gaes = calc_gaes(value_func, ep_states, ep_rewards)
                gaes += list(ep_gaes)

                states += ep_states

                ep_states, ep_rewards = [], []
                state = env.reset()
                reward = 0
        if not done:
            # last episode got cut off by epoch limit
            ep_disc_returns = calc_disc_returns(ep_rewards, GAMMA)
            disc_returns += ep_disc_returns

            ep_gaes = calc_gaes(value_func, ep_states, ep_rewards, state)
            gaes += list(ep_gaes)

            states += ep_states

        states = t.tensor(states, dtype=t.float)
        actions = t.tensor(actions)
        gaes = t.tensor(gaes, dtype=t.float)
        disc_returns = t.tensor(disc_returns, dtype=t.float)

        # Update the policy
        policy.train()
        with t.enable_grad():
            policy_optim.zero_grad()
            prob_dist = policy(states)
            policy_loss = policy_loss_fn(prob_dist, actions, gaes)
            policy_loss.backward()
            policy_optim.step()

        # Fit the value function
        value_func.train()
        for _ in range(CRITIC_ITERS):
            with t.enable_grad():
                value_func_optim.zero_grad()
                state_vals = value_func.forward(states).squeeze()
                value_loss = value_loss_fn(state_vals, disc_returns)
                value_loss.backward()
                # clip_grad_value_(parameters=val_func.parameters(), clip_value=0.1)
                value_func_optim.step()

        if epoch % 5 == 0:
            val_rmse = np.sqrt(value_loss.detach().numpy()).item()
            print(f"Epoch {epoch}: Avg reward = {np.mean(rewards):.3f} Value RMSE = {val_rmse:.3f}")

    print(f"Epoch {epoch}: Avg reward = {np.mean(rewards):.3f}")


if __name__ == "__main__":
    main()
