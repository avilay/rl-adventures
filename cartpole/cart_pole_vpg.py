import gym
import torch as t
import numpy as np
from torch.nn.functional import one_hot, normalize
from torch.distributions import Categorical
from collections import deque

np.set_printoptions(precision=3)

ALPHA = 5e-3
GAMMA = 0.99
STEPS_PER_EPOCH = 4000
EPOCHS = 50
MAX_EP_LEN = 500


class Policy(t.nn.Module):
    def __init__(self):
        super().__init__()
        self.net = t.nn.Sequential(
            t.nn.Linear(in_features=4, out_features=64, bias=True),
            t.nn.PReLU(),
            t.nn.Linear(in_features=64, out_features=64, bias=True),
            t.nn.PReLU(),
            t.nn.Linear(in_features=64, out_features=2, bias=True),
            t.nn.Softmax(),
        )

    def forward(self, states):
        norm_states = normalize(states, dim=1)
        probs = self.net(norm_states)
        return probs

    def sample(self, state):
        batch_of_one = t.tensor([state], dtype=t.float)
        probs = self.forward(batch_of_one)
        action = Categorical(probs=probs).sample().item()
        return action


def loss_fn(prob_dist, actions, value_ests):
    one_hot_actions = one_hot(actions, num_classes=2)
    probs = t.sum(one_hot_actions * prob_dist, axis=1)
    return -t.mean(t.log(probs) * value_ests)


def calc_disc_returns(rewards, gamma):
    disc_cumsums = [None] * len(rewards)
    disc_cumsums[-1] = rewards[-1]
    for i in range(len(rewards) - 2, -1, -1):
        disc_cumsums[i] = rewards[i] + gamma * disc_cumsums[i + 1]
    return disc_cumsums


def main():
    env = gym.make("CartPole-v1")
    rewards = deque([], maxlen=100)
    policy = Policy()
    optim = t.optim.Adam(params=policy.parameters(), lr=ALPHA)

    for epoch in range(EPOCHS):
        # Gather episodes
        states, actions, value_ests = [], [], []
        ep_rewards, ep_avg_rewards = [], []
        state = env.reset()
        reward = 0
        for time_step in range(STEPS_PER_EPOCH):
            action = policy.sample(state)

            states.append(state)
            actions.append(action)
            ep_rewards.append(reward)
            ep_avg_rewards.append(np.mean(ep_rewards))

            state, reward, done, _ = env.step(action)

            if done or len(ep_rewards) == MAX_EP_LEN:
                ep_disc_returns = calc_disc_returns(ep_rewards, GAMMA)
                ep_value_ests = [g - r_bar for g, r_bar in zip(ep_disc_returns, ep_avg_rewards)]
                value_ests += ep_value_ests
                rewards.append(np.sum(ep_rewards))
                ep_rewards, ep_avg_rewards = [], []
                state = env.reset()
                reward = 0
        if not done:
            # last episode got cut off by epoch limit
            ep_disc_returns = calc_disc_returns(ep_rewards, GAMMA)
            ep_value_ests = [g - r_bar for g, r_bar in zip(ep_disc_returns, ep_avg_rewards)]
            value_ests += ep_value_ests

        # Update the policy
        policy.train()
        states = t.tensor(states, dtype=t.float)
        actions = t.tensor(actions)
        value_ests = t.tensor(value_ests, dtype=t.float)
        with t.enable_grad():
            optim.zero_grad()
            prob_dist = policy(states)
            loss = loss_fn(prob_dist, actions, value_ests)
            loss.backward()
            optim.step()

        if epoch % 5 == 0:
            print(f"Epoch {epoch}: Avg reward = {np.mean(rewards):.3f}")


if __name__ == "__main__":
    main()
