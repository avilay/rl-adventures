import numpy as np
import gym

np.set_printoptions(precision=6, floatmode="fixed", suppress=True, linewidth=150)
GAMMA = 0.9


def generate_value_iter():
    env = gym.make("CliffWalking-v0")
    env.reset()
    num_iters = 0

    state_values_prev = np.random.rand(48)
    state_values_cur = np.zeros(48)

    while not np.allclose(state_values_cur, state_values_prev):
        num_iters += 1
        state_values_prev = state_values_cur
        state_values_cur = np.zeros(48)

        # iterate through all the states
        for state in range(48):
            # find the state action value by max of values of each action
            state_action_values = []
            for action in range(env.action_space.n):
                state_action_value = 0
                for p, next_state, reward, done in env.P[state][action]:
                    # next 4 lines are crazy.. we are trying to find out what happens when you cannot get out of the cliff
                    if done:
                        reward = 0
                    if 37 <= state <= 46:
                        next_state = state
                    state_action_value += p * (
                        reward + GAMMA * state_values_prev[next_state]
                    )
                    # state_action_value += (
                    #     GAMMA * p * (reward + state_values_prev[next_state])
                    # )
                state_action_values.append(state_action_value)
            # print(
            #     f"state action values for state {state} are {state_action_values} in iteration {num_iters}. Chosen value is {max(state_action_values)}"
            # )
            state_values_cur[state] = max(state_action_values)

        # print(
        #     f"After iteration {num_iters}, the state_values look like {np.array(state_values_cur).reshape(4, 12)}"
        # )

    print(f"Number of iterations to converge {num_iters}")
    print(np.array(state_values_cur).reshape(4, 12))


if __name__ == "__main__":
    generate_value_iter()
