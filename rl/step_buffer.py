from collections import deque
import numpy as np

"""
# Collect the episode level metrics
                self.tot_reward_episodes.append(np.sum(ep_rewards))
                self.avg_reward_episodes.append(np.mean(ep_rewards))
                self.timesteps_episodes.append(time_step)

                # Calculate the calculated payoff attributes
                start_range = self._ep_start_idxs[-1]
                end_range = self._ep_start_idxs[-1] + ep_timesteps

                ep_disc_returns = calc_disc_cumsum(ep_rewards, self._γ)
                self.disc_returns[start_range:end_range] = ep_disc_returns

                ep_value_ests = calc_value_ests(ep_rewards)
                self.value_ests[start_range:end_range] = ep_value_ests

                ep_adv_ests = calc_adv_ests(value_func, ep_states, ep_rewards)
                self.adv_ests[start_range:end_range] = ep_adv_ests

                ep_gen_adv_ests = calc_gen_adv_ests(value_func, ep_states, ep_rewards)
                self.gen_adv_ests[start_range:end_range] = ep_gen_adv_ests

                self.states[start_range:end_range] = ep_states
                self.rewards[start_range:end_range] = ep_rewards

                self._ep_start_idxs.append(time_step + 1)
"""


def calc_disc_cumsum(values, discount_factor):
    pass


def calc_value_ests(rewards):
    pass


def calc_adv_ests(value_func, states, rewards):
    pass


def calc_gen_adv_ests(value_func, states, rewards):
    pass


class StepBuffer:
    def __init__(self, env, size, max_steps_per_ep, γ, λ):
        self._size = size
        self._env = env
        self._max_steps_per_ep = max_steps_per_ep
        self._γ = γ
        self._λ = λ

        # Buffer attributes
        self.states = None
        self.actions = None
        self.rewards = None

        # Calculated attributes
        self.disc_returns = None
        self.value_ests = None
        self.adv_ests = None
        self.gen_adv_ests = None

        # Buffer metrics
        self.tot_reward_episodes = deque([], maxlen=100)
        self.avg_reward_episodes = deque([], maxlen=100)
        self.timesteps_episodes = deque([], maxlen=100)

        self._ep_start_idxs = None

    def _process_episode_end(self, states, rewards, buf_ptr, value_func, last_state=None):
        start_idx = buf_ptr
        end_idx = buf_ptr + len(states)

        self.disc_returns[start_idx:end_idx] = calc_disc_cumsum(rewards, self._γ)
        self.value_ests[start_idx:end_idx] = calc_value_ests(rewards)
        self.adv_ests[start_idx:end_idx] = calc_adv_ests(value_func, states, rewards)
        self.gen_adv_ests[start_idx:end_idx] = calc_gen_adv_ests(value_func, states, rewards)

    def refresh(self, policy, value_func):
        self.states = np.empty((self._size, *self._env.observation_space.shape))
        self.actions = np.empty(self._size)
        self.rewards = np.empty(self._size)

        self.disc_returns = np.empty(self._size)
        self.value_ests = np.empty(self._size)
        self.adv_ests = np.empty(self._size)
        self.gen_adv_ests = np.empty(self._size)

        buf_ptr = 0
        ep_states, ep_rewards = [], []

        state = self._env.reset()
        for timestep in range(self._size):
            action = policy.sample(state)
            next_state, reward, done, _ = self._env.step(action)

            ep_states.append(state)
            self.actions[timestep] = action
            ep_rewards.append(reward)

            if done or len(ep_rewards) == self._max_steps_per_ep:
                self._process_episode_end(ep_states, ep_rewards, buf_ptr, value_func)
                buf_ptr += len(ep_states)
                ep_states, ep_rewards = [], []
                state = self._env.reset()
            else:
                state = next_state

        if not done:
            # last episode got cut off by epoch limit
            self._process_episode_end(
                ep_states, ep_rewards, buf_ptr, value_func, last_state=next_state
            )
