"""
SFFF
FHFH
FFFH
HFFG
"""
import numpy as np
import gym

np.set_printoptions(precision=6, floatmode="fixed", suppress=True)

LEFT = 0
DOWN = 1
RIGHT = 2
UP = 3
GAMMA = 0.1

# P = gym.make("FrozenLake-v0").env.P
P = gym.make("FrozenLake-v0", is_slippery=False).env.P


def trans_prob(state, action, next_state):
    # return the prob dist of all possible next states
    for p, s_, _, _ in P[state][action]:
        if s_ == next_state:
            return p
    return 0.0


def reward(state, action, next_state):
    for _, s_, r, _ in P[state][action]:
        if s_ == next_state:
            return r
    return 0.0


def optimal_svals():
    state_vals_1 = np.random.rand(16)
    state_vals_2 = np.empty(16)
    num_iters = 0
    while not np.allclose(state_vals_1, state_vals_2):
        num_iters += 1
        state_vals_1 = state_vals_2
        state_vals_2 = np.empty(16)
        for state in range(16):
            state_action_vals = []
            for action in (LEFT, DOWN, RIGHT, UP):
                state_action_val = 0
                for next_state in range(16):
                    state_action_val += (
                        GAMMA
                        * trans_prob(state, action, next_state)
                        * (reward(state, action, next_state) + state_vals_1[next_state])
                    )
                state_action_vals.append(state_action_val)
            state_vals_2[state] = max(state_action_vals)

    print(f"GAMMA = {GAMMA}, number of iters={num_iters}")
    print(np.array(state_vals_1).reshape(4, 4))


if __name__ == "__main__":
    optimal_svals()
